package no.ntnu.idatt2001.patientregister;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegisterTest {
    private PatientRegister register;

    @BeforeEach
    public void init() {
        register = new PatientRegister();
        fillWithTestData();
    }

    private void fillWithTestData() {
        register.addPatient(new Patient("Arne","Jakobsen","Ole V. Eriksen","10019980534"));
        register.addPatient(new Patient("Jon","Aril","Ole V. Eriksen","10027864525"));
        register.addPatient(new Patient("Jon","Torsen","Karl P.","10101010101"));
        register.addPatient(new Patient("Ulrik","Setersby","Karl P.","73478858383"));
        register.addPatient(new Patient("Namil","Karchow","Erik","12331838834"));
        register.addPatient(new Patient("Wilhelm","Stark","Erik","02938747829"));
    }

    @Test
    @DisplayName("Add patient adds a patient")
    public void addingPatientWorks() {
        Patient newPatient = new Patient("Une","Uno","Ole V. Eriksen","79922971723");
        assertDoesNotThrow(() -> register.addPatient(newPatient));
        assertEquals(newPatient,register.get(newPatient));
    }

    @Test
    @DisplayName("Remove patient removes the patient")
    public void removingPatientWorks() {
        Patient patient = new Patient("A","B","D","10019980534"); // Arne Jakobsen
        assertTrue(register.removePatient(patient));
        assertEquals(5,register.getPatients().size());
        assertFalse(register.getPatients().contains(patient));
    }

    @Test
    @DisplayName("Adding duplicate throws exception")
    public void addingDuplicatePatient() {
        Patient newPatient = new Patient("A","B","C","10019980534"); // Arne Jakobsen
        assertThrows(IllegalArgumentException.class, () -> register.addPatient(newPatient));
        assertEquals(6,register.getPatients().size());
    }

    @Test
    @DisplayName("Get patient gets a patient")
    public void getPatientTest() {
        Patient patient = new Patient("A","V","C","10019980534"); // Arne Jakobsen
        assertEquals(patient,register.get(patient));
    }

    @Test
    @DisplayName("Getting a non-existent patient returns null")
    public void getNonExistantPatient() {
        Patient patient = new Patient("A","C","C","11234567890");
        assertNull(register.get(patient));
    }

}

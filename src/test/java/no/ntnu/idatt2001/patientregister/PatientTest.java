package no.ntnu.idatt2001.patientregister;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    private Patient patient;

    @BeforeEach
    public void init() {
        patient = new Patient("Name1", "Name2", "Doctor","12345678910");
    }

    @Test
    @DisplayName("Patient constructor throws exception if data is invalid")
    public void createWrongPatientThrows() {
        assertThrows(IllegalArgumentException.class, () -> new Patient("","","","1"));
        assertThrows(IllegalArgumentException.class, () -> new Patient("","","","asdfghjklas"));
        assertThrows(IllegalArgumentException.class, () -> new Patient("","Name","","12312312345"));
        assertDoesNotThrow((() -> new Patient("Name","Name","","12312312345")));
    }

    @Test
    @DisplayName("Empty diagnosis returns ''")
    public void getEmptyDiagnosis() {
        assertEquals("",patient.getDiagnosis());
    }

    @Test
    @DisplayName("Set name throws if blank")
    public void setBlankName() {
        assertThrows(IllegalArgumentException.class, () -> patient.setFirstName(""));
        assertThrows(IllegalArgumentException.class, () -> patient.setLastName(""));
    }

    @Test
    @DisplayName("Changing patient data works")
    public void changingPatientData() {
        patient.setFirstName("Foo");
        assertEquals("Foo",patient.getFirstName());
        patient.setLastName("Foo");
        assertEquals("Foo",patient.getLastName());
        patient.setGeneralPractitioner("Dr. Peders");
        assertEquals("Dr. Peders", patient.getGeneralPractitioner());

        patient.setFirstName("Not foo");
        assertNotEquals("Foo",patient.getFirstName());
        patient.setLastName("Not foo");
        assertNotEquals("Foo",patient.getLastName());
    }

}

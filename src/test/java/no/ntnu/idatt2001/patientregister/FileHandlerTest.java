package no.ntnu.idatt2001.patientregister;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class FileHandlerTest {
    private FileHandler fileHandler;
    private PatientRegister register;
     private final File file = new File("src\\test\\resources\\no.ntnu.idatt2001.patientregister" +
             "\\PatientsTestResource.csv");
     private final String path = file.getAbsolutePath();



    @BeforeEach
    public void init() {
        fileHandler = new FileHandler();
        register = new PatientRegister();
        fileHandler.clearFile(path);
        fillWithTestData();
        System.out.println(path);
    }

    public void fillWithTestData() {
        register.addPatient(new Patient("A1","A1","A1","11234567890"));
        register.addPatient(new Patient("A2","A2","A2","21234567890"));
        register.addPatient(new Patient("A3","A3","A1","31234567890"));
        register.addPatient(new Patient("A4","A4","A2","41234567890"));
    }

    @Test
    @DisplayName("Writing to file does not throw exception")
    public void writeToFileDoesNotThrow() {
        assertDoesNotThrow(() -> fileHandler.writeCSVFile(register.getPatients(),path));
    }

    @Test
    @DisplayName("Reading from file does not throw exception")
    public void readFromFileDoesNotThrow() {
        assertDoesNotThrow(() -> fileHandler.readCSVFile(path));
    }

    @Test
    @DisplayName("Reading empty file returns empty list")
    public void readingEmptyFile() throws IOException {
        ArrayList<Patient> list = fileHandler.readCSVFile(path);
        assertTrue(list.isEmpty());
    }

    @Test
    @DisplayName("The read list is the same as the written list")
    public void writingThenReading() throws IOException {
        fileHandler.writeCSVFile(register.getPatients(),path);
        ArrayList<Patient> fromFile = fileHandler.readCSVFile(path);
        assertEquals(register.getPatients(),fromFile);
    }

    @Test
    @DisplayName("Patient-data is preserved when saving and reading from file")
    public void dataIsPreserved() throws IOException {
        Patient patient = new Patient("Name","Name 2", "Doctor", "51234567890");
        patient.setDiagnosis("Diagnosis");
        register.addPatient(patient);
        fileHandler.writeCSVFile(register.getPatients(),path);
        ArrayList<Patient> fromFile = fileHandler.readCSVFile(path);
        Patient patientFromFile = fromFile.get(4);
        assertEquals(patient.getFirstName(),patientFromFile.getFirstName());
        assertEquals(patient.getLastName(),patientFromFile.getLastName());
        assertEquals(patient.getGeneralPractitioner(), patientFromFile.getGeneralPractitioner());
        assertEquals(patient.getSocialSecurityNumber(),patientFromFile.getSocialSecurityNumber());
        assertEquals(patient.getDiagnosis(),patientFromFile.getDiagnosis());
        assertEquals(patient,patientFromFile);
    }


}

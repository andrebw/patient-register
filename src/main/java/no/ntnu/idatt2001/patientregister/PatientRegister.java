package no.ntnu.idatt2001.patientregister;

import java.util.ArrayList;

/**
 * This class works as a register of patients. No duplicate can be added.
 * @author andrebw
 */
public class PatientRegister {

    private ArrayList<Patient> patients; // list of patients

    /**
     * Constructor initiates the patients-list.
     */
    public PatientRegister() {
        patients = new ArrayList<>();
    }

    /**
     * Adds a patient to the list if it is not already in the list and it is not null.
     *
     * @param patient to be added.
     * @throws IllegalArgumentException if duplicate is found.
     */
    public void addPatient(Patient patient) throws IllegalArgumentException {
        if (patients.contains(patient)) {
            throw new IllegalArgumentException("Patient already exists in the register: " + get(patient).toString());
        }
        patients.add(patient);
    }

    /**
     * Removes a patient from the list if it exists.
     *
     * @param patient to remove.
     * @return true if the patient was removed, false if not.
     */
    public boolean removePatient(Patient patient) {
        if (patients.contains(patient)) {
            patients.remove(patient);
            return true;
        }
        return false;
    }

    /**
     * Gets a patient from the register.
     *
     * @param patient to find.
     * @return found patient.
     */
    public Patient get(Patient patient) {
        for (Patient p : patients) {
            if (p.equals(patient)) {
                return p;
            }
        }
        return null;
    }

    /**
     * @return a collection of patients.
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Clears the list of patients by deleting all elements, leaving the list empty.
     */
    public void clear() {
        patients.clear();
    }



}

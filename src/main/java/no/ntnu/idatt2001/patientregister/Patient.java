package no.ntnu.idatt2001.patientregister;

import java.util.Objects;

/**
 * This class defines a patient-object.
 * Uses some code from a previous project 'Hospital Manager' (idatt 2001, Mappe: del 1).
 * @author andrebw
 */
public class Patient {

    private String firstName;
    private String lastName;
    private String generalPractitioner;
    private final String socialSecurityNumber;
    private String diagnosis;

    /**
     * The constructor creates an instance of a Patient-object. Fails if social security numbers is not a number.
     *
     * @param firstName patients first name.
     * @param lastName patients last name.
     * @param socialSecurityNumber patients social security number.
     * @throws IllegalArgumentException if social security number is not a number.
     */
    public Patient(String firstName, String lastName,String generalPractitioner, String socialSecurityNumber) throws IllegalArgumentException {
        if (firstName == null || lastName == null || firstName.equals("") || lastName.equals("")) {
            throw new IllegalArgumentException("Name can not be empty.");
        }
        try {
            Long.parseLong(socialSecurityNumber);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Social security number must be a number.");
        }
        if (socialSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Social security number must be 11 digits.");
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.generalPractitioner = generalPractitioner;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " (" +socialSecurityNumber + ")";
    }

    /** (auto-generated)
     * Checks if a object equals this classes object. The check is based on the social security number, which is
     * unique for every person. First and last name is not included since two persons can share the same name.
     *
     * @param o object to be checked
     * @return true if it is equal, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    /** (auto-generated)
     * The hashCode is, as the equals-method, based on the social security number.
     *
     * @return hash code for the object.
     */
    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }

    // getters/setters:
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     * @param firstName to change to.
     * @throws IllegalArgumentException if name is blank.
     */
    public void setFirstName(String firstName) throws IllegalArgumentException {
        if (firstName == null || firstName.equals("")) {
            throw new IllegalArgumentException("First name can not be empty.");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     * @param lastName to change to.
     * @throws IllegalArgumentException if name is blank.
     */
    public void setLastName(String lastName) throws IllegalArgumentException {
        if (lastName == null || lastName.equals("")) {
            throw new IllegalArgumentException("Last name can not be empty.");
        }
        this.lastName = lastName;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getDiagnosis() {
        if (diagnosis != null) {
            return diagnosis;
        }
        return "";
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }


}

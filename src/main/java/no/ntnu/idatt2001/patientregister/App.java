package no.ntnu.idatt2001.patientregister;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 * @author andrebw
 */
public class App extends Application {

    private static Scene scene;
    private final String version = "v0.1-SNAPSHOT";

    @Override
    public void start(Stage stage) throws IOException {
        System.out.println("Starting...");
        scene = new Scene(loadFXML("primary"));
        stage.setTitle("Patient register " + version);
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}
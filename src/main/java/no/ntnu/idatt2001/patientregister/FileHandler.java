package no.ntnu.idatt2001.patientregister;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Class that handles everything with writing and reading Patient-objects to file.
 * @author andrebw
 */
public class FileHandler {


    /**
     * Constructor creates a FileHandler-object.
     */
    public FileHandler() {
    }

    /**
     * Opens a file given a file path and reads line by line. The lines are transformed into patient-objects and added to an ArrayList to be returned.
     *
     * @param filePath path of the file to read.
     * @return ArrayList of patients created form the file.
     * @throws IOException of the file can not be opened.
     */
    public ArrayList<Patient> readCSVFile(String filePath) throws IOException {
        ArrayList<Patient> readPatients = new ArrayList<>();
        try (FileReader fr = new FileReader(filePath)) {

            BufferedReader br = new BufferedReader(fr);

            String line = br.readLine(); // skips the variable-name line (not redundant!)
            line = br.readLine();

            while (line != null) {
                String[] list = line.split(";");
                try {
                    Patient newPatient = new Patient(list[0],list[1],list[2],list[3]);
                    if (list.length == 5) {
                        newPatient.setDiagnosis("" + list[4]); // the given test-resource does not save diagnosis, so
                        // this would fail
                    }
                    readPatients.add(newPatient);
                } catch (IllegalArgumentException e) {
                    System.out.println("Patient could not be created: " + e.getMessage());
                }
                line = br.readLine();
            }
        } catch (IOException e) {
            throw new IOException("File could not be opened at: " + filePath);
        }
        return readPatients;
    }

    /**
     * Opens a file given a file path and writes elements of a list to lines in the file.
     *
     * @param patientList list of patients to write to file.
     * @param filePath path of the file to write to.
     * @throws IOException if the file can not be opened.
     */
    public void writeCSVFile(ArrayList<Patient> patientList, String filePath) throws IOException {
        try (FileWriter fw = new FileWriter(String.valueOf(filePath))) {
            fw.write("firstName;lastName;generalPractitioner;socialSecurityNumber;diagnosis\n");

            for (Patient p : patientList) {
                String line = p.getFirstName() + ";" + p.getLastName() + ";" + p.getGeneralPractitioner() + ";"
                                + p.getSocialSecurityNumber() + ";" + p.getDiagnosis() + "\n";
                fw.append(line);
            }
            System.out.println("Patients saved!");
        } catch (IOException e) {
            throw new IOException("File could not be opened at: " + filePath + "\n" + e.getMessage());
        }
    }

    /**
     * Clears a file at a given file path.
     *
     * @param path of the file.
     */
    public void clearFile(String path) {
        try (FileWriter fw = new FileWriter(path, false)) {
            fw.write("");
            System.out.println("File was cleared at: " + path);
        } catch (IOException e){
            System.out.println("Failed to clear file at: " + path);
            e.printStackTrace();
        }
    }
}

package no.ntnu.idatt2001.patientregister.dialogs;

import javafx.scene.control.Alert;

/**
 * A utility class that opens an information dialog.
 * @author andrebw
 */
public class InformationDialog {

    /**
     * Opens a information dialog with a title and message.
     *
     * @param title of the dialog.
     * @param message of the dialog.
     */
    public static void show(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(message);
        alert.showAndWait();
    }

}

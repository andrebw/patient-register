package no.ntnu.idatt2001.patientregister.dialogs;

import javafx.scene.control.Alert;

/**
 * Utility class that opens a about-dialog.
 * @author andrebw
 */
public class AboutDialog {

    /**
     * Opens about-dialog.
     */
    public static void show() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Patient Register");
        alert.setHeaderText("Patient Register");
        alert.setContentText("A splendid application created by\n" +
                "(C)Andreas Winje\n" +
                "v0.1-SNAPSHOT");
        alert.showAndWait();
    }

}

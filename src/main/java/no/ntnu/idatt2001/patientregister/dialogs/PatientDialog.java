package no.ntnu.idatt2001.patientregister.dialogs;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import no.ntnu.idatt2001.patientregister.NodeFactory;
import no.ntnu.idatt2001.patientregister.Patient;

/**
 * Class that opens a dialog to add or edit a patient.
 * @author andrebw
 */
public class PatientDialog extends Dialog<Patient> {

    /**
     * Mode of the dialog.
     */
    public enum Mode {
        ADD, EDIT
    }

    private final Mode mode;

    private Patient existingPatient = null;

    /**
     * Creates a dialog-object and sets mode to add.
     */
    public PatientDialog() {
        super();
        this.mode = Mode.ADD;
        createDialog();
    }

    /**
     * Creates a dialog-object and sets mode to edit.
     *
     * @param patient to be edited.
     */
    public PatientDialog(Patient patient) {
        super();
        mode = Mode.EDIT;
        this.existingPatient = patient;
        createDialog();
    }

    /**
     * Creates a dialog based on the input. If the constructor has a patient, it opens an edit-dialog, if it does not have a patient it opens a add-new-patinet dialog.
     */
    private void createDialog() {
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        switch (mode) {
            case ADD:
                setTitle("Patient - Add");
                break;
            case EDIT:
                setTitle("Patient - Edit");
                break;
        }
        setupDialog();
    }

    /**
     * Sets up the dialog based on the mode fo the dialog.
     */
    private void setupDialog() {
        NodeFactory factory = new NodeFactory();
        // Note: unsure if I am supposed to cast to the correct sub-class here, but I don't know how else to do it.

        Label firstNameLabel = (Label) factory.create("label");
        firstNameLabel.setText("First name:");
        TextField firstNameInput = (TextField) factory.create("textfield");


        Label lastNameLabel = (Label) factory.create("label");
        lastNameLabel.setText("Last name:");
        TextField lastNameInput = (TextField) factory.create("textfield");


        Label generalPractitionerLabel = (Label) factory.create("label");
        generalPractitionerLabel.setText("General Practitioner: ");
        TextField generalPractitionerInput = (TextField) factory.create("textfield");


        Label socialSecurityNumberLabel = (Label) factory.create("label");
        socialSecurityNumberLabel.setText("Social security number:");
        TextField socialSecurityNumberInput = (TextField) factory.create("textfield");

        Label diagnosisLabel = (Label) factory.create("label");
        diagnosisLabel.setText("Set diagnosis:");
        TextField diagnosisInput = (TextField) factory.create("textfield");



        HBox firstNameBox = (HBox) factory.create("hbox");
        firstNameBox.getChildren().setAll(firstNameLabel,firstNameInput);
        firstNameBox.setSpacing(86);

        HBox lastNameBox = (HBox) factory.create("hbox");
        lastNameBox.getChildren().setAll(lastNameLabel,lastNameInput);
        lastNameBox.setSpacing(86);

        HBox generalPractitionerBox = (HBox) factory.create("hbox");
        generalPractitionerBox.getChildren().setAll(generalPractitionerLabel,generalPractitionerInput);
        generalPractitionerBox.setSpacing(32);

        HBox socialSecurityNumberBox = (HBox) factory.create("hbox");
        socialSecurityNumberBox.getChildren().setAll(socialSecurityNumberLabel,socialSecurityNumberInput);
        socialSecurityNumberBox.setSpacing(20);

        HBox diagnosisBox = (HBox) factory.create("hbox");
        diagnosisBox.getChildren().setAll(diagnosisLabel,diagnosisInput);
        diagnosisBox.setSpacing(70);


        VBox root = (VBox) factory.create("vbox");
        root.setSpacing(10);


        if (mode == Mode.ADD) { // if add:
            root.getChildren().setAll(firstNameBox,lastNameBox,generalPractitionerBox,socialSecurityNumberBox);
            firstNameInput.setPromptText("First name");
            lastNameInput.setPromptText("Last name");
            generalPractitionerInput.setPromptText("General Practitioner");
            socialSecurityNumberInput.setPromptText("Social security number");

            setResultConverter((ButtonType button) -> { // what to return
                Patient patient = null;
                String firstName = firstNameInput.getText().trim();
                String lastName = lastNameInput.getText().trim();
                String generalPractitioner = generalPractitionerInput.getText().trim();
                String socialSecurityNumber = socialSecurityNumberInput.getText().trim();

                if (button == ButtonType.OK) {
                    try {
                        patient = new Patient(firstName,lastName,generalPractitioner,socialSecurityNumber);
                    } catch (IllegalArgumentException e) {
                        InformationDialog.show("Invalid input", "Patient was not added.\n" + e.getMessage());
                    }
                }
                return patient;
            });

        } else { // if edit:
            root.getChildren().setAll(firstNameBox,lastNameBox,generalPractitionerBox,diagnosisBox);
            firstNameInput.setText(existingPatient.getFirstName());
            lastNameInput.setText(existingPatient.getLastName());
            generalPractitionerInput.setText(existingPatient.getGeneralPractitioner());
            diagnosisInput.setText(existingPatient.getDiagnosis());

            setResultConverter((ButtonType button) -> { // what to return
                Patient patient = existingPatient;
                String firstName = firstNameInput.getText().trim();
                String lastName = lastNameInput.getText().trim();
                String generalPractitioner = generalPractitionerInput.getText().trim();
                String diagnosis = diagnosisInput.getText().trim();
                if (button == ButtonType.OK) {
                    try {
                        patient.setFirstName(firstName);
                        patient.setLastName(lastName);
                        patient.setGeneralPractitioner("" + generalPractitioner);
                        patient.setDiagnosis("" + diagnosis);
                    } catch (IllegalArgumentException e) {
                        InformationDialog.show("Invalid input", "Patient was not edited:\n" + e.getMessage());
                    }
                }
                return patient;
            });
        }

        getDialogPane().setContent(root);

    }
}

package no.ntnu.idatt2001.patientregister;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * A class implementing factory-design-pattern to create JavaFX-nodes.
 * @author andrebw
 */
public class NodeFactory {

    /**
     * Creates a node based on a string-input.
     *
     * @param nodeType type of the node to create.
     * @return a node based on the string-input.
     */
    public Node create(String nodeType) {

        if (nodeType.equalsIgnoreCase("IMAGEVIEW")) {
            return new ImageView();
        }
        else if (nodeType.equalsIgnoreCase("MENUBAR")) {
            return new MenuBar();
        }
        else if (nodeType.equalsIgnoreCase("TOOLBAR")) {
            return new ToolBar();
        }
        else if (nodeType.equalsIgnoreCase("BORDERPANE")) {
            return new BorderPane();
        }
        else if (nodeType.equalsIgnoreCase("VBOX")) {
            return new VBox();
        }
        else if (nodeType.equalsIgnoreCase("TABLEVIEW")) {
            return new TableView<>();
        }
        else if (nodeType.equalsIgnoreCase("BUTTON")) {
            return new Button();
        }
        else if (nodeType.equalsIgnoreCase("LABEL")) {
            return new Label();
        }
        else if (nodeType.equalsIgnoreCase("HBOX")) {
            return new HBox();
        } else if (nodeType.equalsIgnoreCase("TEXTFIELD")) {
            return new TextField();
        }

        return null;
    }
}

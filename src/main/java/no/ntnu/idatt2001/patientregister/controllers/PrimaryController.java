package no.ntnu.idatt2001.patientregister.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.idatt2001.patientregister.FileHandler;
import no.ntnu.idatt2001.patientregister.Patient;
import no.ntnu.idatt2001.patientregister.PatientRegister;
import no.ntnu.idatt2001.patientregister.dialogs.AboutDialog;
import no.ntnu.idatt2001.patientregister.dialogs.InformationDialog;
import no.ntnu.idatt2001.patientregister.dialogs.PatientDialog;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Optional;

/**
 * The main controller of the application. Handles all button presses in the main window.
 * @author andrebw
 */
public class PrimaryController {

    @FXML
    private Label statusLabel; // label that displays status of imports/exports
    @FXML
    private TableView<Patient> tableView;
    @FXML
    private TableColumn<Patient,String> firstNameColumn, lastNameColumn, generalPractitionerColumn,
    socialSecurityNumberColumn, diagnosisColumn; // TableView-columns

    ObservableList<Patient> patientsObservable = FXCollections.observableArrayList(); // the observable list attached
                                                                                      // to the TableView
    PatientRegister patients = new PatientRegister(); // list of patients

    private Patient selectedPatient;

    /**
     * Initializes the controller.
     */
    @FXML
    private void initialize() {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));

        tableView.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY) || mouseEvent.getButton().equals(MouseButton.SECONDARY)) {
                selectedPatient = tableView.getSelectionModel().getSelectedItem();
                System.out.println("Selected item: " + selectedPatient);
            }
        });

        tableView.setItems(patientsObservable);

        statusLabel.setText("OK");
    }

    private void updateObservableList() {
        this.patientsObservable.setAll(patients.getPatients());
    }

    /**
     * Opens a patient-dialog and creates a Patient-object from the user-input, then adds the patient to the patient register and updates display.
     */
    @FXML
    private void onAddPatient() {
        PatientDialog dialog = new PatientDialog();

        Optional<Patient> result = dialog.showAndWait();

        if (result.isPresent()) {
            Patient newPatient = result.get();

            try {
                patients.addPatient(newPatient);
                updateObservableList();
                System.out.println("Patient \"" + newPatient + "\" added!");
            } catch (IllegalArgumentException e) {
                InformationDialog.show("Patient not added",e.getMessage());
                System.out.println("Patient not added: " + e.getMessage());
            }

        } else {
            System.out.println("Patient adding cancelled.");
        }
    }

    /**
     * Gets the selected patient and deletes it if the user answers 'OK' on the confirmation-dialog.
     */
    @FXML
    private void onRemoveSelectedPatient() {

        if(selectedPatient != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete Confirmation");
            alert.setHeaderText("Delete Confirmation");
            alert.setContentText("Are you sure you want to delete patient \n\"" + selectedPatient + "\" from the " +
                    "register?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                patients.removePatient(selectedPatient);
                updateObservableList();
                System.out.println("Removed patient \"" + selectedPatient + "\" from the register");
                selectedPatient = null;
            } else {
                System.out.println("Patient deletion cancelled.");
            }
        }
    }

    /**
     * Gets the selected patient and opens the edit-patient-dialog with it. Saves the changes and updates the list.
     */
    @FXML
    private void onEditSelectedPatient() {
        if (selectedPatient != null) {
            PatientDialog dialog = new PatientDialog(selectedPatient);

            Optional<Patient> result = dialog.showAndWait();

            if (result.isPresent()) {
                selectedPatient = result.get();
                updateObservableList();
            } else {
                System.out.println("Patient editing cancelled.");
            }
        }
    }

    /**
     * Opens a FileChooser-dialog and gets the filePath of the chosen file, then reads the chosen file. The register
     * is then cleared and the elements from the file is then added to the register and the display is updated.
     * It only allows import from .csv-files.
     */
    @FXML
    private void onImportFromCSV() {
        FileHandler fileHandler = new FileHandler();
        FileChooser fileChooser = getFileChooser();
        File file = fileChooser.showOpenDialog(new Stage());
        ArrayList<Patient> duplicates = new ArrayList<>(); // if there are any duplicates, they are added here

        if (file != null) {
            ArrayList<Patient> readPatients;
            try {
                patients.clear();
                readPatients = fileHandler.readCSVFile(file.getAbsolutePath());
                for (Patient p : readPatients) {
                    try {
                        patients.addPatient(p);
                    } catch (IllegalArgumentException e) {
                        System.out.println("Patient \"" + p + "\" not added: " + e.getMessage());
                        duplicates.add(p);
                    }
                }
                updateObservableList();
                statusLabel.setText("Import successful!");
                System.out.println("Import successful!");
                /* In future, get all the duplicates and ask the user which ones they want to overwrite with, but for
                now an alert is sufficient: */
                if (!duplicates.isEmpty()) {
                    InformationDialog.show("Duplicate patient found",duplicates.size() + " patient(s) were not added" +
                            " because a patient with the same social security numbers already exist in the register.");
                }
            } catch (IOException e) {
                statusLabel.setText("Error: file was not imported.");
                e.printStackTrace();
            }
        } else {
            System.out.println("Import from file cancelled.");
        }
    }

    /**
     * Opens a FileChooser-dialog and opens the file at the filePath, writes the PatientRegister to the file.
     * It only allows export to .csv-files.
     */
    @FXML
    private void onExportToCSV() {
        FileHandler fileHandler = new FileHandler();
        FileChooser fileChooser = getFileChooser();
        File file = fileChooser.showSaveDialog(new Stage());

        if(file != null) {
            try {
                fileHandler.writeCSVFile(patients.getPatients(), file.getAbsolutePath());
                statusLabel.setText("Export successful!");
                System.out.println("Export successful!");
            } catch (IOException e) {
                statusLabel.setText("Error: file was not exported.");
                e.printStackTrace();
            }
        } else {
            System.out.println("Export to file cancelled");
        }
    }

    /**
     * Clears the table and deletes everything.
     */
    @FXML
    private void onClearTable() {
        if (patients.getPatients().size() > 0) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Clear table");
            alert.setHeaderText("Clear table");
            alert.setContentText("Are you sure you want to clear the table? All unsaved data will be lost.");

            Optional<ButtonType> answer = alert.showAndWait();
            if (answer.isPresent() && answer.get() == ButtonType.OK) {
                patients.clear();
                updateObservableList();
                System.out.println("Table cleared.");
            } else {
                System.out.println("Cancelled clear table.");
            }
        }
    }

    /**
     * Opens about-dialog.
     */
    @FXML
    private void onAbout() {
        System.out.println("Showing about");
        AboutDialog.show();
    }

    /**
     * Exits the program if the user presses 'OK' on the confirm-dialog.
     */
    @FXML
    private void onExit() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit program");
        alert.setHeaderText("Exit program");
        alert.setContentText("Are you sure you want to exit the program?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            Platform.exit();
        }
    }

    /**
     * Gets a FileChooser with given parameters.
     *
     * @return a FileChooser.
     */
    private FileChooser getFileChooser() {
        // could not get the getResource-method to work...
        // default directory (resource folder)
        String path = FileSystems.getDefault().getPath("").toAbsolutePath() +
                "\\src\\main\\resources\\no\\ntnu\\idatt2001\\patientregister";
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(path));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(".csv", "*.csv"));

        return fc;
    }
}

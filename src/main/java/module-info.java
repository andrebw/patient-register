module no.ntnu.idatt2001.andrebw.patientregister {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.idatt2001.patientregister.controllers to javafx.fxml;
    exports no.ntnu.idatt2001.patientregister.controllers;
    exports no .ntnu.idatt2001.patientregister;
}